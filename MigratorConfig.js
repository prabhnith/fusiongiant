module.exports = {
    database: {
        client: 'sqlite3',
        connection: {
            filename: 'contents/data/ghost.db'
        }
    },
    migrationPath: process.cwd() + '/core/server/data/migrations',
    currentVersion: '1.0'
}
